Wiley tests
The project is designed to test the performance of the wiley UI

Running the tests
mvn test

If you want to set browser, you need to use:
mvn test -Pchrome
mvn test -Pfirefox
