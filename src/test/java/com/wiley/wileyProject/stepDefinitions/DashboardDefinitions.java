package com.wiley.wileyProject.stepDefinitions;

import com.wiley.wileyProject.pageObjects.common.CommonElements;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

import static com.codeborne.selenide.Selenide.open;

public class DashboardDefinitions extends CommonElements {

    @Given("^user opens \"([^\"]*)\"$")
    public void openWebPortal(String url) {
        open(url);
    }

    @When("^the user closes model dialog$")
    public void closeModelDialog() {

    }

    @When("^All links must be visibility$")
    public void checkLinksIsVisible() {

    }
}
