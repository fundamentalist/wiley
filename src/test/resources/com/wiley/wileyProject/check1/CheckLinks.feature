Feature: Check Resources, Subjects and About links

  Scenario: User checks the links are displayed in the top menu
    Given user opens "https://www.wiley.com/en-us"
    When the user closes model dialog
#    When check resources link exists
#    When check subjects link exists
#    When check about link exists
    Then All links must be visibility

#  Scenario: User checks items under Resources for sub-header
#    Given getAction1Valid
#    When the user opens resource link
#    When the user watches Students item
#    When the user watches Instructors item
#    When the user watches Researchers item
#    When the user watches Professionals item
#    When the user watches Librarians item
#    When the user watches Institutions item
#    When the user watches Authors item
#    When the user watches Resellers item
#    When the user watches Corporations item
#    When the user watches Societies item
#    Then there are 10 items under resources sub-header
#    Then Close window in check2